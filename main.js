/*Використання асинхронності дозволяє уникнути блокування інтерфейсу користувача та інших процесів в браузері.
Основна ідея полягає в тому, що під час виконання завдань, які можуть займати тривалий час, наприклад, завантаження даних з сервера, інша частина коду може продовжити виконуватися.
Це дозволяє програмам більш ефективно взаємодіяти з іншими ресурсами та виконувати довготривалі завдання, не перериваючи роботу іншої частини коду.*/

async function findIP() {
    try {
        const ipResponse = await fetch("https://api.ipify.org/?format=json");
        const ipData = await ipResponse.json();
        const clientIP = ipData.ip;

        const locationResponse = await fetch(`http://ip-api.com/json/${clientIP}?fields=continent,country,regionName,city,district`);
        const locationData = await locationResponse.json();

        const resultDiv = document.getElementById("result");
        resultDiv.innerHTML = `
            <h2>Результат:</h2>
            <p>Континент: ${locationData.continent || 'Не знайдено'}</p>
            <p>Країна: ${locationData.country || 'Не знайдено'}</p>
            <p>Регіон: ${locationData.regionName || 'Не знайдено'}</p>
            <p>Місто: ${locationData.city || 'Не знайдено'}</p>
            <p>Район: ${locationData.district || 'Не знайдено'}</p>
        `;

    } catch (error) {
        console.error("Помилка при отриманні даних:", error);
    }
}